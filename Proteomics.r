######################################
library(MetaboAnalystR)
library(ggplot2)
library(stringr)
library(reshape2)
library(dplyr)

mSet<-InitDataObjects("pktable", "stat", FALSE)
mSet<-Read.TextData(mSet, "Proteomics.csv", "colu", "disc")
mSet<-SanityCheckData(mSet)

#mSet <- ImputeVar(mSet, method="colmin")
# normalization 


mSet<-ImputeMissingVar(mSet, method="exclude")

mSet<-PreparePrenormData(mSet)
mSet<-Normalization(mSet, "NULL", "NULL", "AutoNorm", ratio=FALSE, ratioNum=20)

mSet<-PlotNormSummary(mSet, "metabolites_norm", "png", 300, width=NA)
mSet<-PlotSampleNormSummary(mSet, "sample_norm", "png", 300, width=NA)


# normality test

x <- mSet$dataSet$norm
means <- data.frame(apply(x, 2, mean))
shapiro.test(means$apply.x..2..mean.)
plot(density(means$apply.x..2..mean.))
library(ggpubr)
ggqqplot(means$apply.x..2..mean.)

library(nortest)
sf.test(means$apply.x..2..mean.)  #Shapiro-Francia test for normality


#metabolites
before_norom <- data.frame( mSet$dataSet$proc )
before_norom <- data.frame(apply(before_norom, 2, mean, na.rm = TRUE))
colnames(before_norom) <- "value"

qq_before <- ggplot(before_norom , aes(sample  = value) ) + 
  geom_qq(size = 2.5 , alpha= 0.7) + geom_qq_line(size = 0.7, colour="red" ) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top") +
  
  theme(text=element_text(size=16, face = "bold")) +
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white")) + 
  ylab("Intensity") + xlab("Theoretical")

ggsave("before_norm_metabolites_qqplot.png",qq_before, dpi = 300, width = 9, height = 5.5)


#######################


after_norom <- data.frame( mSet$dataSet$norm )
after_norom <- data.frame(apply(after_norom, 2, mean, na.rm = TRUE))
colnames(after_norom) <- "value"

qq_after <- ggplot(after_norom , aes(sample  = value) ) + 
  geom_qq(size = 2.5 , alpha= 0.7) + geom_qq_line(size = 0.7, colour="red" ) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top") +
  
  theme(text=element_text(size=16, face = "bold")) +
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white")) + 
  ylab("Intensity") + xlab("Theoretical")

ggsave("after_norm_metabolites_qqplot.png",qq_after, dpi = 300, width = 9, height = 5.5)


####sample

before_norom <- data.frame( mSet$dataSet$proc )
before_norom <- data.frame(apply(before_norom, 1, mean, na.rm = TRUE))
colnames(before_norom) <- "value"

qq_before <- ggplot(before_norom , aes(sample  = value) ) + 
  geom_qq(size = 2.5 , alpha= 0.7) + geom_qq_line(size = 0.7, colour="red" ) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top") +
  
  theme(text=element_text(size=16, face = "bold")) +
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white")) + 
  ylab("Intensity") + xlab("Theoretical")

ggsave("before_norm_Samples_qqplot.png",qq_before, dpi = 300, width = 9, height = 5.5)

#######################


after_norom <- data.frame( mSet$dataSet$norm )
after_norom <- data.frame(apply(after_norom, 1, mean, na.rm = TRUE))
colnames(after_norom) <- "value"

qq_after <- ggplot(after_norom , aes(sample  = value) ) + 
  geom_qq(size = 2.5 , alpha= 0.7) + geom_qq_line(size = 0.7, colour="red" ) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top") +
  
  theme(text=element_text(size=16, face = "bold")) +
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white")) + 
  ylab("Intensity") + xlab("Theoretical")

ggsave("after_norm_Sample_qqplot.png",qq_after, dpi = 300, width = 9, height = 5.5)


#######################

# 1- Fold Change Analysis

mSet<-FC.Anal(mSet, 1.5, 1, FALSE) #autism/control
#mSet<-PlotFC(mSet, "foldchange_cutoff_2", "png", 150, width=NA)
######## soudy
x <- mSet$analSet$fc
y <- data.frame(fc_log = x$fc.log)

yall <- y
row.names(yall) <- NULL
yall$idu <- as.numeric(row.names(yall))

yall$color <- ifelse(yall$fc_log > log2(1.5), "Up-regulated",
                     ifelse( yall$fc_log < -log2(1.5), "Down-regulated" ,"No-change" ))


yall$color <- factor(yall$color , levels = c("Up-regulated", "No-change", "Down-regulated"))

x <- ggplot(yall , aes(y = fc_log , x = idu, colour = color)) + 
  
  geom_point(size = 3, alpha = 0.7) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top") +
  
  theme(text=element_text(size=16, face = "bold")) +
  
  scale_color_manual(values=c( "#D11300", "#6b6b6b",  "#0070D1")) + 
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white")) + 
  scale_fill_discrete(name = "New Legend Title") + 
  
  geom_hline(yintercept=0, size=1.5, alpha=0.7)+
  geom_hline(yintercept=log2(1.5), size=1.5, linetype="dashed", alpha=0.7) +
  geom_hline(yintercept=-log2(1.5), size=1.5, linetype="dashed", alpha=0.7) + 
  
  theme(axis.text.x=element_blank(), axis.line.x=element_blank(),
        axis.ticks.x=element_blank() , legend.title=element_blank()) +
  
  xlab("Proteins") +
  ylab("Log2[FC(SARS Cov 2/CTRL)]") + scale_y_continuous(sec.axis = sec_axis(~./1)) 

ggsave("Fold change.jpeg",x, dpi = 300 , width = 9, height = 5.5)

# 2- T Tests
mSet<-Ttests.Anal(mSet, T, 0.05, FALSE, TRUE, "raw", TRUE)

#mSet<-PlotTT(mSet, "t_test_adjusted_pvalue_unequal_paramtric", "png", 150, width=NA)
# Soudy

x <- read.csv("wilcox_rank_all.csv",row.names = 1)

Logpvalue <- x$X.log10.p.

jpeg(filename = paste0("t-test1.jpeg") , width = 3000 , height = 1200,res = 300)
plot(Logpvalue  , xaxt='n' ,xlab = "Proteins" , ylab = "-log10(P-value)" , col = ifelse(Logpvalue >= 1.3,'darkred','grey') , pch = 19)
dev.off()

###################
x <- read.csv("wilcox_rank_all.csv",row.names = 1)
x$IDs <- rownames(x)
rownames(x) <-NULL
x$idu <- as.numeric(row.names(x))
xall <- x
#xall$
xall$color <- ifelse(xall$X.log10.p. > -log10(0.05) , "P_value < 0.05", "P_value > 0.05")

xall_annot <- xall[xall$color == "P_value < 0.05",]

x <- ggplot(xall , aes(y = X.log10.p. , x = idu, colour = color)) + 
  
  geom_point(size = 3) + 
  

  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top" , legend.title=element_blank()) +
  
  theme(text=element_text(size=16, face = "bold")) +
  
  scale_color_manual(values=c("#0070D1", "#D11300")) + 
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white")) + 
  scale_fill_discrete(name = "") +
  geom_hline(yintercept= -log10(0.05), size=1.5, linetype="dashed", alpha=0.7) +
  theme(axis.text.x=element_blank(), axis.line.x=element_blank(),
        axis.ticks.x=element_blank()) + 
  xlab("Metabolites") +
  ylab("-log10(P-value)") + scale_y_continuous(sec.axis = sec_axis(~./1)) 


  
ggsave("W_test.jpeg",x, dpi = 300, width = 12, height = 10)


# 3 - Volcano Plot (raw p-value)


mSet<-Volcano.Anal(mSet, FALSE, 1.5, 1, F, 0.1, TRUE, "fdr")


x <- mSet$analSet$volcano
x <- data.frame(p_log= x$p.log, fc_log = x$fc.log)

x$color <- ifelse( x$p_log > -log10(0.1) & (x$fc_log > log2(1.5)  | x$fc_log < -log2(1.5)) , "0.5 FoldChange", 
                   "Non Significant"  )


x$color <- factor(x$color, levels = c("0.5 FoldChange", "Non Significant"))

xminma <- min(x$fc_log) - 0.5
xmaxma <- max(x$fc_log) + 0.5

yminma <- min(x$fc_log) 
ymaxma <- max(x$fc_log) + 0.5

volcano <- ggplot(x, aes(x= fc_log, y= p_log , colour = color))+xlab('log2(FC)') + ylab('-log10(q-value)')+
  geom_point(size = 1.2 , alpha = 0.7)+ geom_rug(alpha=0.6)+  
  theme_bw() + theme(legend.title=element_blank())+ 
  geom_vline(xintercept = -log2(1.5), linetype = "dotted")+
  geom_vline(xintercept = 0)+geom_hline(yintercept = -log10(0.1))+
  geom_vline(xintercept = log2(1.5) , linetype = "dotted")+ xlab("Log2 (SARS Cov 2/CTRL)") +
  scale_color_manual(values = c("#CB4335", "#2C3E50")) 


ggsave("Volcano Plotmod real data.jpeg" ,plot = volcano , dpi = 300 , width = 7 , height = 4)


# 4- PCA 2D

mSet<-PCA.Anal(mSet)
#mSet<-PlotPCA2DScore(mSet, "PCA_2D", "png", 150, width=NA, 1,2,0.95,0,0)


PCA_data <- read.csv("pca_score.csv" , row.names = 1)

vars <- apply(PCA_data, 2, var)
PC1_var <- round( ( var(PCA_data$PC1) / sum(vars) ) * 100, 1)
PC2_var <- round( ( var(PCA_data$PC2) / sum(vars) ) * 100, 1)
PC3_var <- round( ( var(PCA_data$PC3) / sum(vars) ) * 100, 1)

PCA_data$sample <- rownames(PCA_data)
PCA_data$color <- ifelse(grepl("SARS", PCA_data$sample,fixed = TRUE), "SARS Cov 2", "CTRL")
PCA_data$sample <- NULL

x <- ggplot(PCA_data , aes( x = PC1, y = PC2, colour = color)) + 
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white") , 
          panel.border = element_rect(colour = "black", fill=NA, size=1, 
                                      linetype = "solid") , 
          
          panel.grid = element_line(colour = "grey", size = 0.1)) +

  stat_ellipse(aes(x=PC1, y=PC2, fill=color) ,level=0.95,geom="polygon",alpha=0.1) +
  
  geom_point(aes(colour=color, fill=color) ,alpha = 0.7, 
             shape = 21,size = 4,colour = "black" , stroke = 1.5)+
  
  scale_fill_manual(values=c( "#ff0000" ,"#00cc00")) +
  
  scale_color_manual(values=c( "#ff0000" ,"#00cc00")) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top" , legend.title=element_blank()) +
  
  theme(text=element_text(size=16, face = "bold")) + 
  
  ylab(paste0("PC2 ", "(", PC2_var , "%", ")")) + 
  xlab(paste0("PC1 ", "(", PC1_var , "%" ,")"))

  
  
ggsave("PCA_2d.png",x, dpi = 300, width = 8, height = 7)

  
# 5- PCA 3d

mSet<-PCA.Anal(mSet)

PCA_data <- read.csv("pca_score.csv" , row.names = 1)

vars <- apply(PCA_data, 2, var)
PC1_var <- round( ( var(PCA_data$PC1) / sum(vars) ) * 100, 1)
PC2_var <- round( ( var(PCA_data$PC2) / sum(vars) ) * 100, 1)
PC3_var <- round( ( var(PCA_data$PC3) / sum(vars) ) * 100, 1)

PCA_data$sample <- rownames(PCA_data)
PCA_data$color <- ifelse(grepl("hcc", PCA_data$sample,fixed = TRUE), "#ff0000", "#00cc00")
PCA_data$sample <- NULL


library("scatterplot3d") # load

tiff('PCA_3D2.tiff', res  = 400 , units="in", width = 10, height = 7 )

s3d <- scatterplot3d(x = PCA_data$PC1, y=PCA_data$PC2, z = PCA_data$PC3,
              angle = 380,
              xlab = paste0("PC1 ", "(", PC1_var , "%" ,")") , 
              ylab = paste0("PC2 ", "(", PC2_var , "%" ,")") , 
              zlab = paste0("PC3 ", "(", PC3_var , "%" ,")"),
              color = PCA_data$color,
              grid=TRUE, box=FALSE , 
              pch = 16, type="h",
              cex.symbols = 1.5)

legend("topright",s3d$xyz.convert(18, 0, 12),
       col = c("#ff0000", "#00cc00"),
       pch=c(16,16), yjust=0,
       # here you define the labels in the legend
       legend = c("HCC", "NASH"), cex = 2, inset = 0.15
)

dev.off()
# 6- PLSDA

mSet<-PLSR.Anal(mSet, reg=TRUE)
mSet<-PlotPLS2DScore(mSet, "PLSDA_metabo", "png", 150, width=NA, 1,2,0.95,0,0)


PCA_data <- read.csv("plsda_score.csv" , row.names = 1)

vars <- mSet$analSet$plsr$Xtotvar
PC1_var <- round( ( mSet$analSet$plsr$Xvar[1] / vars ) * 100, 1)
PC2_var <- round( ( mSet$analSet$plsr$Xvar[2] / vars ) * 100, 1)
PC3_var <- round( ( mSet$analSet$plsr$Xvar[3] / vars ) * 100, 1)

PCA_data$sample <- rownames(PCA_data)
PCA_data$color <- ifelse(grepl("SARS", PCA_data$sample,fixed = TRUE), "SARS Cov 2", "CTRL")
PCA_data$sample <- NULL


write.csv(PCA_data, "plsda_nash.csv")
x <- ggplot(PCA_data , aes( x = Comp.1, y = Comp.2, colour = color)) + 
  
  theme(  panel.background = element_rect(fill = "white",
                                          colour = "white") , 
          panel.border = element_rect(colour = "black", fill=NA, size=1, 
                                      linetype = "solid") , 
          
          panel.grid = element_line(colour = "grey", size = 0.1)) +
  
  stat_ellipse(aes(x=Comp.1, y=Comp.2, fill=color) ,level=0.95,geom="polygon",alpha=0.1) +
  
  geom_point(aes(colour=color, fill=color), alpha = 0.7, 
             shape = 21,size = 4,colour = "black" , stroke = 1.5)+
  
  scale_fill_manual(values=c( "#ff0000" ,"#00cc00")) +
  
  scale_color_manual(values=c( "#ff0000" ,"#00cc00")) +
  
  theme( axis.line = element_line(colour = "black", 
                                  size = 1, linetype = "solid")) + 
  
  theme(legend.background = element_rect(size=0.5, linetype="solid", 
                                         colour ="black")) + 
  
  theme(legend.position="top" , legend.title=element_blank()) +
  
  theme(text=element_text(size=16, face = "bold")) + 
  
  xlab(paste0("Component 1 ", "(", PC1_var , "%", ")")) + 
  ylab(paste0("Component 2 ", "(", PC2_var , "%" ,")"))


ggsave("PLSDA_2D.png",x, dpi = 300, width = 8, height = 7)

library(pls)

mSet<-PLSDA.CV(mSet, "T",5, "Q2")

mSet<-PlotPLS.Classification(mSet, "pls_cv", "png", 300, width=NA)


mSet<-PlotPLS.Imp(mSet, "pls_VIM", "png", 300, width=NA, "vip", "Comp. 1", 15,FALSE)

mSet<-PLSDA.Permut(mSet, 100, "bw")

mSet<-PlotPLS.Permutation(mSet, "pls_perm", "png", 300, width=NA)


###### heat_map with features

mSet<-Ttests.Anal(mSet, F, 0.1, FALSE, TRUE, "fdr", FALSE)

x <- mSet$analSet$tt$p.value
x <- data.frame(x)
x <- x[x$x < 0.05,]
#mSet<-PlotSubHeatMap(mSet, "heatmap_features", "png", 300, width=NA, "norm", "row", "correlation", "ward.D","bwm", "tanova", 25, "overview", T, T, T, F)


var.nms = colnames(mSet$dataSet$norm)

mSet_heat <- mSet

feauture_number <- 30
if (feauture_number < length(var.nms)) {
  if ("tanova" == "tanova") {
    if (2 == 2) {
      if (is.null(mSet_heat$analSet$tt)) {
        Ttests.Anal(mSet_heat)
      }
      var.nms <- names(sort(mSet_heat$analSet$tt$p.value))[1:feauture_number]
    }
    else {
      if (is.null(mSet_heat$analSet$aov)) {
        ANOVA.Anal(mSet_heat)
      }
      var.nms <- names(sort(mSet_heat$analSet$aov$p.value))[1:feauture_number]
    }
  }
}

data_heatmap_feature <- mSet$dataSet$norm[,colnames(mSet$dataSet$norm) %in% var.nms]

library("pheatmap")
library("dichromat")
library("RColorBrewer")
library(viridis)

#vol <- read.csv("volcano.csv")
#vol <- vol[vol$log2.FC. >= 0.5 , ]
#data_heatmap_feature <- data_heatmap_feature[,colnames(data_heatmap_feature) %in% vol$X]

data_heatmap_feature$group <- ifelse(grepl("SARS",rownames(data_heatmap_feature)),
                                     "SARS Cov 2","CTRL")

group <- data.frame( Group = data_heatmap_feature$group, 
                     row.names = row.names(data_heatmap_feature))

data_heatmap_feature$group <- NULL

#my_colour = list(group = c(HCC = "red", NASH = "grenn"))

colnames(group)
my_colour = list(
  Group = c(`SARS Cov 2` = "green",  CTRL = "red"))



for (i in colnames(data_heatmap_feature)){print(i)}

# namess <- read.csv("uniprot.tab", sep = "\t")
# 
# namess <- namess[namess$Query %in% names(data_heatmap_feature),]
# names(data_heatmap_feature)[match(namess[,"Query"], names(data_heatmap_feature))] = namess[,"Match"]

data_heatmap_feature <- data.frame(t(data_heatmap_feature))

pheatmap(data_heatmap_feature , border_color = "black" , 
         cellwidth = 33, cellheight = 33, scale = "column", 
         clustering_distance_rows = "correlation", 
         clustering_distance_cols = "correlation" , 
         clustering_method = "ward.D",
         fontsize = 20,  color =  rev(colorRampPalette(brewer.pal(11, "RdGy"))(256)),
         angle_col = 45 , na_col = "grey",
         cluster_cols = T , cluster_rows = T,
         height =  20, width = 35 , 
         filename = "features_heatmap_Final2.png",
         annotation_col  = group,
         annotation_names_col  = F,
         annotation_colors = my_colour,
         cutree_rows = 2,
         cutree_cols= 2)




###################
####
